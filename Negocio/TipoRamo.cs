﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class TipoRamo
    {
        public int id { get; set; }
        public string nombre { get; set; }

        public List<TipoRamo> get()
        {
            var dt = Datos.DataAccess.GetInstance();

            List<TipoRamo> list = dt.getQuery<TipoRamo>("select * from TipoRamo", new List<SqlParameter>());
            return list;
        }

    }
}
