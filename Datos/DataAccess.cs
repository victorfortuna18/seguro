﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class DataAccess
    {
        string conn = ConfigurationManager.ConnectionStrings["db"].ToString();

        private static DataAccess instance;

        private DataAccess(){}

        public static DataAccess GetInstance()
        {
            if (instance == null) instance = new DataAccess();
            return instance; 
        }

        public List<T> getQuery<T>(string query, List<SqlParameter> parameters)
        {
            SqlConnection cnn = new SqlConnection(conn);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = query;

            parameters.ForEach(delegate (SqlParameter parameter)
            {
                cmd.Parameters.Add(parameter);
            });

            cnn.Open();
            SqlDataReader o = cmd.ExecuteReader();

            List<T> trans = new List<T>();
            trans = DataReaderMapToList<T>(o);
            cnn.Close();

            return trans;
        }


        public void setQuery(string query, List<SqlParameter> parameters)
        {
            SqlConnection cnn = new SqlConnection(conn);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = query;

            parameters.ForEach(delegate (SqlParameter parameter)
            {
                cmd.Parameters.Add(parameter);
            });

            cnn.Open();
            cmd.ExecuteNonQuery();
            cnn.Close();
        }

        public int setQueryEscalar(string query, List<SqlParameter> parameters)
        {
            SqlConnection cnn = new SqlConnection(conn);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = query;

            parameters.ForEach(delegate (SqlParameter parameter)
            {
                cmd.Parameters.Add(parameter);
            });

            cnn.Open();
            //cmd.ExecuteNonQuery();
            var id = (int)cmd.ExecuteScalar();
            cnn.Close();

            return id;
        }

        public List<T> getDynamicSP<T>(string sp, List<SqlParameter> parameters)
        {
            SqlConnection cnn = new SqlConnection(conn);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = sp; 
            
            parameters.ForEach(delegate (SqlParameter parameter)
            {
                cmd.Parameters.Add(parameter);
            });

            cnn.Open();
            SqlDataReader o = cmd.ExecuteReader();

            List<T> trans = new List<T>();
            trans = DataReaderMapToList<T>(o);
            cnn.Close();

            return trans;
        }

       
        public void setDynamicSP(string sp, List<SqlParameter> parameters)
        {
            SqlConnection cnn = new SqlConnection(conn);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = sp; 
            
            parameters.ForEach(delegate (SqlParameter parameter)
            {
                cmd.Parameters.Add(parameter);
            });

            cnn.Open();
            cmd.ExecuteNonQuery();
            cnn.Close();
        }

        public static List<T> DataReaderMapToList<T>(System.Data.IDataReader dr)
        {

            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (DataRecordExtensions.HasColumn(dr, prop.Name))
                        if (!object.Equals(dr[prop.Name], DBNull.Value))
                        {
                            prop.SetValue(obj, dr[prop.Name], null);
                        }
                }
                list.Add(obj);
            }
            return list;
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
               
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                 
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
    }

    public static class DataRecordExtensions
    {
        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }
    }
}
