﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class Cotizacion
    {
        public int id { get; set; }
        public int idRamo { get; set; }
        public string nombrePersona { get; set; }

        public List<Cotizacion> get()
        {
            var dt = Datos.DataAccess.GetInstance();

            List<Cotizacion> list = dt.getQuery<Cotizacion>("select * from Cotizacion", new List<SqlParameter>());
            return list;
        }

        public void save()
        {
            var dt = Datos.DataAccess.GetInstance();

            //var query = "insert into Cotizacion (idRamo,nombrePersona) values (@idRamo,@nombrePersona); select scope_identity();";
            var query = "insert into Cotizacion (idRamo,nombrePersona) output INSERTED.ID values (@idRamo,@nombrePersona); ";

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@idRamo", idRamo));
            parameters.Add(new SqlParameter("@nombrePersona", nombrePersona));

            id = dt.setQueryEscalar(query , parameters);
            
        }
    }   
}
