USE [master]
GO

CREATE DATABASE Seguro;
GO

USE [Seguro]
GO

CREATE TABLE [dbo].[Cotizacion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idRamo] [int] NULL,
	[nombrePersona] [nvarchar](250) NULL,
 CONSTRAINT [PK_Cotizacion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CotizacionLine](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCotizacion] [int] NULL,
	[idTasa] [int] NULL,
	[descripcion] [nvarchar](250) NULL,
	[prima] [decimal](18, 2) NULL,
 CONSTRAINT [PK_CotizacionLine] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Ramo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idTipoRamo] [int] NULL,
	[nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_Ramo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[TipoRamo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_TipoRamo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[Tasa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NULL,
	[monto] [decimal](18, 2) NULL,
	[idTipoRamo] [int] NULL,
 CONSTRAINT [PK_Tasa] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


INSERT INTO [dbo].[Ramo] ([idTipoRamo],[nombre]) VALUES (1, 'Plan personal');
INSERT INTO [dbo].[Ramo] ([idTipoRamo],[nombre]) VALUES (2, 'Plan general');
GO

INSERT INTO [dbo].[TipoRamo] ([nombre]) VALUES ('Personal');
INSERT INTO [dbo].[TipoRamo] ([nombre]) VALUES ('General');
GO

INSERT INTO [dbo].[Tasa] ([nombre],[monto],[idTipoRamo]) VALUES ('Vida',0.08,1);
INSERT INTO [dbo].[Tasa] ([nombre],[monto],[idTipoRamo]) VALUES ('Accidentes personales',0.02,1);
INSERT INTO [dbo].[Tasa] ([nombre],[monto],[idTipoRamo]) VALUES ('Auto',0.15,2);
INSERT INTO [dbo].[Tasa] ([nombre],[monto],[idTipoRamo]) VALUES ('Incendio',0.20,2);
GO