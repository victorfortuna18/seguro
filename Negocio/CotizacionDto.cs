﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class CotizacionDto
    {
        public string nombrePersona { get; set; }
        public int idRamo { get; set; }
        public decimal sumaAsegurada { get; set; }
        public decimal tasa { get; set; }
        public decimal valorCotizacion { get; set; }

        public void calcular()
        {
            valorCotizacion = sumaAsegurada * tasa;
        }
    }
}
