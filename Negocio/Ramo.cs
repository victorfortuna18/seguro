﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class Ramo
    {
        public int id { get; set; }
        public int idTipoRamo { get; set; }
        public string nombre { get; set; }

        public List<Ramo> get(int id = 0)
        {
            var dt = Datos.DataAccess.GetInstance();

            var query = "select * from Ramo";

            List<SqlParameter> parameters = new List<SqlParameter>();

            if (id != 0)
            {
                parameters.Add(new SqlParameter("@id", id));
                query += " where id = @id";
            }

            List<Ramo> list = dt.getQuery<Ramo>(query, parameters);
            return list;
        }
    }
}
