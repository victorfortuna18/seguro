﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class CotizacionLine
    {
        public int id { get; set; }
        public int idCotizacion { get; set; }
        public int idTasa { get; set; }
        public string descripcion { get; set; }
        public decimal prima { get; set; }

        public List<CotizacionLine> get()
        {
            var dt = Datos.DataAccess.GetInstance();

            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@idCotizacion", idCotizacion));

            List<CotizacionLine> list = dt.getQuery<CotizacionLine>("select * from CotizacionLine where idCotizacion = @idCotizacion", parameters);
            return list;
        }

        public void save()
        {
            var dt = Datos.DataAccess.GetInstance();

            var query = "insert into CotizacionLine " +
                " (idCotizacion,idTasa,descripcion,prima) " +
                " values (@idCotizacion,@idTasa,@descripcion,@prima)";

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@idCotizacion", idCotizacion));
            parameters.Add(new SqlParameter("@idTasa", idTasa));
            parameters.Add(new SqlParameter("@descripcion", descripcion));
            parameters.Add(new SqlParameter("@prima", prima));

            dt.setQuery(query, parameters);

        }

        public void calcular(decimal sumaAsegurada = 0)
        {
            var tasas = new Tasa().get(idTasa);
            var tasa = (tasas.Count > 0)? tasas[0] : new Tasa();
           

            prima = sumaAsegurada * tasa.monto;
        }
    }
}
