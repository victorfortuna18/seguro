﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Seguro.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult FormularioCotizacion()
        {
            //ViewBag.tasas = new Negocio.Tasa().get();
            ViewBag.ramos = new Negocio.Ramo().get();
            // ViewBag.tipoRamos = new Negocio.TipoRamo().get();

            return PartialView();
        }

        [HttpPost]
        public ActionResult ProcesarCotizacion(string nombrePersona, decimal sumaAsegurada, int idRamo)
        {
            //cotizacionDto.calcular();
            var cotizacion = new Negocio.Cotizacion()
            {
                idRamo = idRamo,
                nombrePersona = nombrePersona
            };
            cotizacion.save();

            if(cotizacion.id != 0)
            {
                var ramos = new Negocio.Ramo().get(cotizacion.idRamo);
                var ramo = (ramos.Count > 0)? ramos[0] : new Negocio.Ramo();

                var tasas = new Negocio.Tasa().get(0,ramo.idTipoRamo);

                foreach (Negocio.Tasa tasa in tasas)
                {
                    var cotizacionLine = new Negocio.CotizacionLine()
                    {
                        idCotizacion = cotizacion.id,
                        idTasa = tasa.id,
                        descripcion = tasa.nombre
                    };
                    cotizacionLine.calcular(sumaAsegurada);
                    cotizacionLine.save();
                }
            }

            return View("Index");
        }

        public ActionResult Cotizaciones()
        {

            ViewBag.cotizaciones = new Negocio.Cotizacion().get();
            return PartialView();
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}