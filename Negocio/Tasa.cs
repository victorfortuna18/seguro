﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class Tasa
    {
        public int id { get; set; }
        public int idTipoRamo { get; set; }
        public string nombre { get; set; }
        public decimal monto { get; set; }

        public List<Tasa> get(int id = 0, int idTipoRamo = 0)
        {
            var dt = Datos.DataAccess.GetInstance();

            var query = "select * from Tasa" ;

            List<SqlParameter> parameters = new List<SqlParameter>();
            
            if (id != 0)
            {
                parameters.Add(new SqlParameter("@id", id));
                query += " where id = @id";
            } else if (idTipoRamo != 0)
            {
                parameters.Add(new SqlParameter("@idTipoRamo", idTipoRamo));
                query += " where idTipoRamo = @idTipoRamo";
            }

            List<Tasa> list = dt.getQuery<Tasa>(query, parameters);
            return list;
        }
    }
}
